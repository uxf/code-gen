<?php

declare(strict_types=1);

namespace UXF\CodeGen\Util;

use Doctrine\Common\Collections\Collection;
use ReflectionIntersectionType;
use ReflectionNamedType;
use ReflectionUnionType;
use UXF\CMS\Entity\User;

final readonly class EntityHelper
{
    private const array UNSUPPORTED_RETURN_TYPE_NAMES = ['array', Collection::class, 'self', User::class];
    
    public static function supportedReturnType(ReflectionNamedType|ReflectionUnionType|ReflectionIntersectionType|null $returnType): bool
    {
        if (!$returnType instanceof ReflectionNamedType) {
            return false;
        }
        $returnTypeName = $returnType->getName();
        if (in_array($returnTypeName, self::UNSUPPORTED_RETURN_TYPE_NAMES, true)) {
            return false;
        }
        
        return true;
    }
}
