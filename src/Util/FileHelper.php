<?php

declare(strict_types=1);

namespace UXF\CodeGen\Util;

use function Safe\file_put_contents;

final readonly class FileHelper
{
    public static function writeFile(
        string $dstFile,
        string $content,
    ): void {
        if (is_file($dstFile)) {
            echo "\n--------------------------------\n";
            echo "WARNING: File was not generated!\n";
            echo "File '$dstFile' already exists.\n";
            echo "--------------------------------\n";

            return;
        }
        file_put_contents($dstFile, $content);
    }
}
