<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Symfony\Component\Routing\Attribute\Route;
use UXF\CodeGen\Http\Request\ControllerRequestBody;
use UXF\CodeGen\Util\FileHelper;

use function Safe\mkdir;

final readonly class ControllerService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(ControllerRequestBody $controllerRequestBody): void
    {
        $zoneName = $controllerRequestBody->zoneName;
        $entityName = $controllerRequestBody->entityName;

        // names
        $entityLc = lcfirst($entityName);
        $entityPlural = $entityName . 's';

        $repositoryName = "{$entityName}Repository";
        $repositoryClass = "\App\\$zoneName\Repository\\{$repositoryName}";
        $repositoryProperty = lcfirst($repositoryName);

        $responseName = "{$entityName}Response";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Controller\\$entityName");

        $namespace->addUse("App\\$zoneName\Entity\\$entityName")
            ->addUse("App\\$zoneName\Repository\\{$repositoryName}")
            ->addUse("App\\$zoneName\Http\Response\\{$responseName}")
            ->addUse(Route::class);

        $class = $namespace->addClass("Get{$entityPlural}Controller")->setFinal()->setReadOnly();

        $zoneNameUS = EntityService::entityNameToTableName($zoneName);
        $zoneNameDS = EntityService::entityNameToAlias($zoneName);
        $entityNameUS = EntityService::entityNameToTableName($entityPlural);
        $entityNameDS = EntityService::entityNameToAlias($entityName);

        // constructor
        $class->addMethod('__construct')
            ->setPublic()
            ->addPromotedParameter($repositoryProperty)
            ->setType($repositoryClass)
            ->setPrivate();

        // index
        $class->addMethod('__invoke')
            ->setPublic()
            ->setReturnType('array')
            ->setBody("
                 return array_map(
    static fn ($entityName \${$entityLc}) => $responseName::createFromEntity(\$$entityLc),
    \$this->{$repositoryProperty}->findAll()
);")
            ->addComment("@return {$responseName}[]")
            ->addAttribute(Route::class, [
                'path' => "/api/$zoneNameDS/$entityNameDS",
                'name' => "api_{$zoneNameUS}_get_{$entityNameUS}",
                'methods' => 'GET',
            ]);

        // print file
        $printer = new PsrPrinter();
        $repositoryOut = $printer->printFile($phpFile);

        $controllerDir = $this->srcDirectory . "{$zoneName}/Controller/$entityName";
        if (!is_dir($controllerDir)) {
            mkdir($controllerDir, 0755, true);
        }

        $dstFile = $this->srcDirectory . "{$zoneName}/Controller/$entityName/Get{$entityPlural}Controller.php";
        FileHelper::writeFile($dstFile, $repositoryOut);
    }
}
