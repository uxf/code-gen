<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use ReflectionClass;
use ReflectionNamedType;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;
use UXF\CodeGen\Http\Request\TypeRequestBody;
use UXF\CodeGen\Util\EntityHelper;
use UXF\CodeGen\Util\FileHelper;

use UXF\CodeGen\Util\ParseHelper;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;
use UXF\Storage\GQL\Type\File;
use UXF\Storage\GQL\Type\Image;
use function Safe\mkdir;
use function Safe\preg_match;

final readonly class TypeService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(TypeRequestBody $typeRequestBody): void
    {
        $zoneName = $typeRequestBody->zoneName;
        $entityName = $typeRequestBody->entityName;
        $entityData = $this->processEntityData($zoneName, $entityName);
        $className = "{$entityName}Type";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\GQL\Type");

        $namespace
            ->addUse(name: "App\\$zoneName\Entity\\$entityName")
            ->addUse(DateTime::class)
            ->addUse(Date::class)
            ->addUse(Time::class)
            ->addUse(Field::class)
            ->addUse(Type::class);

        foreach ($entityData as $entityRecord) {
            $returnType = $entityRecord['returnType'];
            $isResponse = $entityRecord['isResponse'];
            if ($isResponse) {
                $namespace->addUse($returnType);
            }
        }

        $class = $namespace
            ->addClass($className)
            ->addAttribute(Type::class, [
                'name' => $entityName,
            ]);
        $class->setFinal()->setReadOnly();

        // create constructor with promoted parameters
        $constructor = $class
            ->addMethod('__construct')
            ->setPublic();

        foreach ($entityData as $entityRecord) {
            $propertyName = $entityRecord['propertyName'];
            $returnType = $entityRecord['returnType'];
            $returnNullable = $entityRecord['returnNullable'];
            $constructor
                ->addPromotedParameter($propertyName)
                ->setType($returnType)
                ->setNullable($returnNullable)
                ->addAttribute(Field::class);
        }

        // create method "createFromEntity"
        $createFromEntity = $class
            ->addMethod('createFromEntity')
            ->setPublic()
            ->setStatic()
            ->setReturnType('self');

        $entityLc = lcfirst($entityName);
        $createFromEntity->addParameter($entityLc)
            ->setType("\App\\$zoneName\Entity\\$entityName");

        $properties = [];
        foreach ($entityData as $entityRecord) {
            $getterName = $entityRecord['getterName'];
            $returnType = $entityRecord['returnType'];
            $isResponse = $entityRecord['isResponse'];
            $returnNullable = $entityRecord['returnNullable'];
            if ($isResponse) {
                if ($returnNullable) {
                    $properties[] = "    \\$returnType::createNullable(\$$entityLc->$getterName()),";
                } else {
                    $properties[] = "    \\$returnType::createFromEntity(\$$entityLc->$getterName()),";
                }
            } else {
                $properties[] = "    \$$entityLc->$getterName(),";
            }
        }
        $createFromEntity->addBody("return new self(\n" . implode("\n", $properties) . "\n);");

        // create method "createNullable"
        $createNullable = $class
            ->addMethod('createNullable')
            ->setPublic()
            ->setStatic()
            ->setReturnNullable()
            ->setReturnType('self');

        $createNullable->addParameter($entityLc)
            ->setNullable()
            ->setType("\App\\$zoneName\Entity\\$entityName");

        $createNullable->addBody("return \$$entityLc !== null ? self::createFromEntity(\$$entityLc) : null;");

        // print class to file
        $printer = new PsrPrinter();
        $typeOut = $printer->printFile($phpFile);

        $typeDir = $this->srcDirectory . "{$zoneName}/GQL/Type";
        if (!is_dir($typeDir)) {
            mkdir($typeDir, 0755, true);
        }

        $dstFile = "{$typeDir}/{$className}.php";
        FileHelper::writeFile($dstFile, $typeOut);
    }

    /**
     * @return array<array{getterName: string, propertyName: string, returnType: string, returnNullable: bool, returnBuiltin: bool, isResponse: bool}>
     */
    private function processEntityData(string $zoneName, string $entityName): array
    {
        /** @var class-string $className */
        $className = "App\\$zoneName\Entity\\$entityName";

        $rc = new ReflectionClass($className);

        $methods = $rc->getMethods();
        $methods = array_filter($methods, static fn ($method) => str_starts_with($method->getName(), 'get'));
        $entityData = [];
        foreach ($methods as $method) {
            $getterName = $method->getName();
            $propertyName = ParseHelper::convertGetterToPropertyName($getterName);
            $returnType = $method->getReturnType();
            if (!$returnType instanceof ReflectionNamedType) {
                continue;
            }

            if (EntityHelper::supportedReturnType($returnType) === false) {
                continue;
            }

            if ($returnType->getName() === 'UXF\Storage\Entity\Image') {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => Image::class,
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => true,
                ];
            } elseif ($returnType->getName() === 'UXF\Storage\Entity\File') {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => File::class,
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => true,
                ];
            } elseif (preg_match('/\\\Entity\\\/m', $returnType->getName()) === 1) {
                // je to entita, vazba ManyToOne , musim se podivat zda neexistuje Type pro tuto entitu
                // pokud existuje, pouzij Type, pokud neexistuje vytvor novy Type

                $zoneName = ParseHelper::parseZoneNameFromClassName($returnType->getName());
                $entityName = ParseHelper::parseEntityFromClassName($returnType->getName());
                echo "- $getterName / $propertyName / $returnType / $zoneName / $entityName\n";

                $expectedResponseFile = $this->srcDirectory . "$zoneName/GQL/Type/{$entityName}Type.php";

                if (!is_file($expectedResponseFile)) {
                    $this->generate(new TypeRequestBody($zoneName, $entityName));
                }
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => "App\\$zoneName\\GQL\\Type\\{$entityName}Type",
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => true,
                ];
            } else {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => $returnType->getName(),
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => false,
                ];
            }
        }

        return $entityData;
    }
}
