<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use TheCodingMachine\GraphQLite\Annotations\Query;
use UXF\CodeGen\Http\Request\QueryRequestBody;
use UXF\CodeGen\Util\FileHelper;
use UXF\Core\Attribute\Entity;
use function Safe\mkdir;

final readonly class QueryService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(QueryRequestBody $queryRequestBody): void
    {
        $zoneName = $queryRequestBody->zoneName;
        $entityName = $queryRequestBody->entityName;

        // names
        $entityLc = lcfirst($entityName);
        $entityClass = "App\\$zoneName\Entity\\$entityName";

        $typeName = "{$entityName}Type";
        $typePhp = "\App\\{$zoneName}\\GQL\Type\\{$typeName}";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\GQL\Query");

        $namespace->addUse($entityClass)
            ->addUse($typePhp)
            ->addUse(Query::class)
            ->addUse(Entity::class);

        $class = $namespace->addClass("{$entityName}Query")->setFinal()->setReadOnly();

        // constructor
        $class->addMethod('__construct')
            ->setPublic();

        // index
        $method = $class->addMethod('__invoke')
            ->setPublic()
            ->setReturnType($typePhp)
            ->setBody("
                 return $typeName::createFromEntity(\$$entityLc);")
            ->addAttribute(Query::class, [
                'name' => lcfirst($entityName),
            ]);
        $method
            ->addParameter($entityLc)
            ->addAttribute(Entity::class)
            ->setType($entityClass);

        // print file
        $printer = new PsrPrinter();
        $queryOut = $printer->printFile($phpFile);

        $queryDir = $this->srcDirectory . "{$zoneName}/GQL/Query";
        if (!is_dir($queryDir)) {
            mkdir($queryDir, 0755, true);
        }

        $dstFile = "{$queryDir}/{$entityName}Query.php";
        FileHelper::writeFile($dstFile, $queryOut);
    }
}
