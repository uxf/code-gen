<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use Nette\Utils\Strings;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;
use UXF\CodeGen\Http\Request\EntityRequestBody;
use UXF\CodeGen\Util\FileHelper;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;
use UXF\Storage\Entity\File;
use UXF\Storage\Entity\Image;

final readonly class EntityService
{
    public const array DATA_TYPES = [
        'string' => [
            'typePhp' => 'string',
        ],
        'text' => [
            'typePhp' => 'string',
        ],
        'datetime' => [
            'typePhp' => DateTime::class,
            'typeOrm' => DateTime::class,
        ],
        'date' => [
            'typePhp' => Date::class,
            'typeOrm' => Date::class,
        ],
        'time' => [
            'typePhp' => Time::class,
            'typeOrm' => Time::class,
        ],
        'boolean' => [
            'typePhp' => 'bool',
        ],
        'integer' => [
            'typePhp' => 'int',
        ],
        'smallint' => [
            'typePhp' => 'integer',
        ],
        'bigint' => [
            'typePhp' => 'integer',
        ],
        'float' => [
            'typePhp' => 'float',
        ],
        'guid' => [
            'typePhp' => 'string',
        ],
        'ManyToOne' => [
            'typePhp' => null,
        ],
        'File' => [
            'typePhp' => File::class,
        ],
        'Image' => [
            'typePhp' => Image::class,
        ],
    ];

    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(EntityRequestBody $entityDto): void
    {
        $zoneName = $entityDto->zoneName;
        $entityName = $entityDto->entityName;
        $entityLabel = $entityDto->entityLabel;
        $columns = $entityDto->columns;

        $dataTypes = self::DATA_TYPES;
        // convert entity name to table name, entity alias and schema name
        $entityAlias = self::entityNameToAlias($entityName);
        $schemaName = self::zoneNameToSchemaName($zoneName);

        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Entity");
        $namespace
            ->addUse('Doctrine\ORM\Mapping', 'ORM')
            ->addUse(CmsMeta::class)
            ->addUse(CmsGrid::class)
            ->addUse(CmsForm::class);

        $class = $namespace->addClass($entityName);

        $class->addAttribute(Entity::class)
            ->addAttribute(Table::class, [
                'schema' => "app_{$schemaName}",
            ])
            ->addAttribute(CmsMeta::class, [
                'alias' => $entityAlias,
                'title' => $entityLabel,
            ]);

        $class->addProperty('id')
            ->setType('int')
            ->setPrivate()
            ->setValue(0)
            ->setNullable()
            ->addAttribute(Column::class)
            ->addAttribute(Id::class)
            ->addAttribute(GeneratedValue::class, [
                'strategy' => 'IDENTITY',
            ])
            ->addAttribute(CmsGrid::class, [
                'label' => 'Id',
            ])
            ->addAttribute(CmsForm::class, [
                'hidden' => true,
            ]);

        // column properties
        foreach ($columns as $column) {
            $label = $column->label;
            $name = $column->name;
            $dbType = $column->dbType;
            $nullable = $column->nullable;
            if (array_key_exists($dbType, $dataTypes)) {
                $typePhp = $dataTypes[$dbType]['typePhp'];
            } else {
                $typePhp = $dbType;
            }

            $typeOrm = $dbType;
            if (isset($dataTypes[$dbType]['typeOrm'])) {
                $typeOrm = new Literal('\\' . $dataTypes[$dbType]['typeOrm'] . '::class');
            }

            if ($dbType === 'ManyToOne') {
                $inversedBy = lcfirst($entityName . 's');
                $m2oZoneName = $column->many2oneZoneName;
                $m2oEntityName = $column->many2oneEntityName;

                $namespace->addUse("App\\$m2oZoneName\\Entity\\$m2oEntityName");
                $class->addProperty($name)
                    ->setType("\App\\$m2oZoneName\\Entity\\$m2oEntityName")
                    ->setPrivate()
                    ->setValue(null)
                    ->setNullable($nullable)
                    ->addAttribute(ManyToOne::class, [
                        'inversedBy' => $inversedBy,
                    ])
                    ->addAttribute(JoinColumn::class, [
                        'nullable' => $nullable,
                    ])
                    ->addAttribute(CmsGrid::class, [
                        'label' => $label,
                        'targetColumn' => 'name',
                    ])
                    ->addAttribute(CmsForm::class, [
                        'label' => $label,
                        'targetField' => 'name',
                    ])
                    ->setInitialized($nullable);
            } elseif ($dbType === 'File') {
                $namespace->addUse(File::class);
                $class->addProperty($name)
                    ->setType(File::class)
                    ->setPrivate()
                    ->setValue(null)
                    ->setNullable($nullable)
                    ->addAttribute(ManyToOne::class)
                    ->addAttribute(JoinColumn::class, [
                        'nullable' => $nullable,
                    ])
                    ->addAttribute(CmsGrid::class, [
                        'label' => $label,
                        'targetColumn' => 'name',
                    ])
                    ->addAttribute(CmsForm::class, [
                        'label' => $label,
                        'targetField' => 'name',
                    ])
                    ->setInitialized($nullable);
            } elseif ($dbType === 'Image') {
                $namespace->addUse(Image::class);
                $class->addProperty($name)
                    ->setType(Image::class)
                    ->setPrivate()
                    ->setValue(null)
                    ->setNullable($nullable)
                    ->addAttribute(ManyToOne::class)
                    ->addAttribute(JoinColumn::class, [
                        'nullable' => $nullable,
                    ])
                    ->addAttribute(CmsGrid::class, [
                        'label' => $label,
                        'targetColumn' => 'name',
                    ])
                    ->addAttribute(CmsForm::class, [
                        'label' => $label,
                        'targetField' => 'name',
                    ])
                    ->setInitialized($nullable);
            } else {
                $property = $class->addProperty($name)
                    ->setType($typePhp)
                    ->setPrivate()
                    ->setNullable($nullable)
                    ->setInitialized($nullable)
                    ->addAttribute(Column::class, [
                        'type' => $typeOrm,
                        'nullable' => $nullable,
                    ])
                    ->addAttribute(CmsGrid::class, [
                        'label' => $label,
                    ])
                    ->addAttribute(CmsForm::class, [
                        'label' => $label,
                    ]);

                if ($nullable) {
                    $property->setValue(null);
                }
            }
        }

        // constructor
        $constructor = $class->addMethod('__construct')
            ->setPublic();

        // getter for Id
        $class->addMethod('getId')
            ->setPublic()
            ->setReturnType('int')
            ->setReturnNullable(false)
            ->setBody('return $this->id ?? 0;');

        // ### getters and setters ###
        foreach ($columns as $column) {
            $name = $column->name;
            $dbType = $column->dbType;
            $nullable = $column->nullable;
            $m2oZoneName = $column->many2oneZoneName;
            $m2oEntityName = $column->many2oneEntityName;

            if ($dbType === 'ManyToOne') {
                $typePhp = "\App\\$m2oZoneName\\Entity\\$m2oEntityName";
            } elseif (array_key_exists($dbType, $dataTypes)) {
                $typePhp = $dataTypes[$dbType]['typePhp'];
            } else {
                $typePhp = $dbType;
            }

            $nameUC = ucfirst($name);

            // getter
            $class->addMethod("get$nameUC")
                ->setPublic()
                ->setReturnType($typePhp)
                ->setReturnNullable($nullable)
                ->setBody("return \$this->$name;");

            // setter
            $setter = $class->addMethod("set$nameUC")
                ->setPublic()
                ->setReturnType('self')
                ->addBody("\$this->$name = \$$name;")
                ->addBody('return $this;');

            $setter->addParameter($name)
                ->setType($typePhp)
                ->setNullable($nullable);

            // Constructor - set param in constructor if param is not nullable
            if (!$nullable) {
                $constructor->addBody("\$this->$name = \$$name;");

                $constructor->addParameter($name)
                    ->setType($typePhp)
                    ->setNullable($nullable);
            }
        }

        // write file
        $printer = new PsrPrinter();
        $entityOut = $printer->printFile($phpFile);
        $dstFile = $this->srcDirectory . "$zoneName/Entity/$entityName.php";
        FileHelper::writeFile($dstFile, $entityOut);

        /*
         * One To Many relations
         * =====================
         * this part only print recommendation for inverse side
         */
        foreach ($columns as $column) {
            $mappedBy = $column->name;
            $dbType = $column->dbType;
            $nullable = $column->nullable;
            $m2oZoneName = $column->many2oneZoneName;
            $m2oEntityName = $column->many2oneEntityName;

            $propertyName = lcfirst("{$entityName}s");
            $getterName = "get{$entityName}s";

            if ($dbType === 'ManyToOne') {
                $class = new ClassType($m2oEntityName);
                $class->addProperty($propertyName)
                    ->setPrivate()
                    ->addComment(
                        "@var Collection<int,\App\\$zoneName\\Entity\\$entityName>",
                    )
                    ->addAttribute(OneToMany::class, [
                        'targetEntity' => "App\\$zoneName\\Entity\\$entityName",
                        'mappedBy' => $mappedBy,
                    ]);

                $class->addMethod($getterName)
                    ->setPublic()
                    ->setReturnType(Collection::class)
                    ->setReturnNullable($nullable)
                    ->addComment(
                        "@return Collection<int,\App\\$zoneName\\Entity\\$entityName>",
                    )
                    ->setBody("return \$this->$propertyName;");

                $printer = new PsrPrinter();

                echo "For inverse side add property and getter to entity: $m2oZoneName\Entity\\$m2oEntityName";
                echo $printer->printClass($class);
            }
        }
    }

    public static function entityNameToTableName(string $entityName): string
    {
        return mb_strtolower(Strings::replace($entityName, ['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2'));
    }

    public static function zoneNameToSchemaName(string $zoneName): string
    {
        return mb_strtolower(Strings::replace($zoneName, ['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1'));
    }

    public static function entityNameToAlias(string $entityName): string
    {
        return mb_strtolower(Strings::replace($entityName, ['/([a-z\d])([A-Z])/', '/([^-])([A-Z][a-z])/'], '$1-$2'));
    }
}
