<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use ReflectionClass;
use UXF\CodeGen\Http\Request\QueryRequestBody;
use UXF\CodeGen\Util\FileHelper;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;
use function Safe\mkdir;
use function Safe\preg_match;

final readonly class GqlTestService
{
    public function __construct(private string $projectDirectory)
    {
    }

    public function generate(QueryRequestBody $queryRequestBody): void
    {
        $zoneName = $queryRequestBody->zoneName;
        $entityName = $queryRequestBody->entityName;

        // names
        $entityLc = lcfirst($entityName);

        $typeClassName = "{$entityName}Type";
        $typePhp = "\App\\{$zoneName}\\GQL\Type\\{$typeClassName}";

        $inputClassName = "{$entityName}Input";
        $inputPhp = "\App\\{$zoneName}\\GQL\Input\\$inputClassName";

        $queryClassName = "{$entityName}Query";
        $queryGqlName = "{$entityLc}";

        $queryListClassName = "{$entityName}sQuery";
        $queryListGqlName = "{$entityLc}s";

        $mutationCreateClassName = "{$entityName}CreateMutation";
        $mutationCreateGqlName = "{$entityLc}Create";

        $mutationUpdateClassName = "{$entityName}UpdateMutation";
        $mutationUpdateGqlName = "{$entityLc}Update";

        // test
        $testDir = "{$this->projectDirectory}/tests/{$zoneName}/GQL";
        $testMutationDir = "{$testDir}/Mutation";
        $testQueryDir = "{$testDir}/Query";
        $testNamespace = "App\Tests\\{$zoneName}\\{$entityName}";

        $testCreateFile = "{$testDir}/{$entityName}CreateMutationStoryTest.php";
        $testCreateClassName = "{$entityName}CreateMutationStoryTest";
        $testUpdateFile = "{$testDir}/{$entityName}UpdateMutationStoryTest.php";
        $testUpdateClassName = "{$entityName}UpdateMutationStoryTest";
        $testQueryFile = "{$testDir}/{$entityName}QueryStoryTest.php";
        $testQueryClassName = "{$entityName}QueryStoryTest";
        $testQueryListFile = "{$testDir}/{$entityName}sQueryStoryTest.php";
        $testQueryListClassName = "{$entityName}sQueryStoryTest";

        // create directories
        if (!is_dir($testDir)) {
            mkdir($testDir, 0755, true);
        }
        if (!is_dir($testMutationDir)) {
            mkdir($testMutationDir, 0755, true);
        }
        if (!is_dir($testQueryDir)) {
            mkdir($testQueryDir, 0755, true);
        }
        $constructorBody = self::getInputConstructorExampleData($inputPhp);

        // CREATE TEST
        // generate output php test class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace($testNamespace);
        $namespace
            ->addUse($inputPhp)
            ->addUse(Date::class)
            ->addUse(DateTime::class)
            ->addUse(Time::class)
            ->addUse($inputPhp)
            ->addUse("App\Tests\StoryTestCase");
        $class = $namespace->addClass($testCreateClassName)->setExtends("App\Tests\StoryTestCase");
        // testSuccess
        $class->addMethod('testSuccess')
            ->setPublic()
            ->setReturnType("void")
            ->setBody("
\$client = \$this->getWebClient();
\$client->login();

\$this->gql(__DIR__ . '/Mutation/{$mutationCreateClassName}.graphql', [
    'input' => new $inputClassName(
        $constructorBody
    ),
]);
            ");
        // print test file
        $printer = new PsrPrinter();
        $testOut = $printer->printFile($phpFile);
        FileHelper::writeFile($testCreateFile, $testOut);

        // UPDATE TEST
        // generate output php test class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace($testNamespace);
        $namespace
            ->addUse($inputPhp)
            ->addUse(Date::class)
            ->addUse(DateTime::class)
            ->addUse(Time::class)
            ->addUse($inputPhp)
            ->addUse("App\Tests\StoryTestCase");
        $class = $namespace->addClass($testUpdateClassName)->setExtends("App\Tests\StoryTestCase");
        // testSuccess
        $class->addMethod('testSuccess')
            ->setPublic()
            ->setReturnType("void")
            ->setBody("
\$client = \$this->getWebClient();
\$client->login();

\$this->gql(__DIR__ . '/Mutation/{$mutationCreateClassName}.graphql', [
    'input' => new $inputClassName(
        $constructorBody
    ),
]);

\$this->gql(__DIR__ . '/Mutation/{$mutationUpdateClassName}.graphql', [
    '$entityLc' => 1,
    'input' => new $inputClassName(
        $constructorBody
    ),
]);
            ");
        // print test file
        $printer = new PsrPrinter();
        $testOut = $printer->printFile($phpFile);
        FileHelper::writeFile($testUpdateFile, $testOut);

        // QUERY TEST
        // generate output php test class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace($testNamespace);
        $namespace
            ->addUse($inputPhp)
            ->addUse(Date::class)
            ->addUse(DateTime::class)
            ->addUse(Time::class)
            ->addUse($inputPhp)
            ->addUse("App\Tests\StoryTestCase");
        $class = $namespace->addClass($testQueryClassName)->setExtends("App\Tests\StoryTestCase");
        // testSuccess
        $class->addMethod('testSuccess')
            ->setPublic()
            ->setReturnType("void")
            ->setBody("
\$client = \$this->getWebClient();
\$client->login();

\$this->gql(__DIR__ . '/Mutation/{$mutationCreateClassName}.graphql', [
    'input' => new $inputClassName(
        $constructorBody
    ),
]);

\$this->gql(__DIR__ . '/Query/{$queryClassName}.graphql', [
    '$entityLc' => 1,
]);
            ");
        // print test file
        $printer = new PsrPrinter();
        $testOut = $printer->printFile($phpFile);
        FileHelper::writeFile($testQueryFile, $testOut);

        // TEST QUERY LIST
        // generate output php test class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace($testNamespace);
        $namespace
            ->addUse($inputPhp)
            ->addUse(Date::class)
            ->addUse(DateTime::class)
            ->addUse(Time::class)
            ->addUse($inputPhp)
            ->addUse("App\Tests\StoryTestCase");
        $class = $namespace->addClass($testQueryListClassName)->setExtends("App\Tests\StoryTestCase");
        // testSuccess
        $class->addMethod('testSuccess')
            ->setPublic()
            ->setReturnType("void")
            ->setBody("
\$client = \$this->getWebClient();
\$client->login();

\$this->gql(__DIR__ . '/Mutation/{$mutationCreateClassName}.graphql', [
    'input' => new $inputClassName(
        $constructorBody
    ),
]);

\$this->gql(__DIR__ . '/Query/{$queryListClassName}.graphql');
            ");
        // print test file
        $printer = new PsrPrinter();
        $testOut = $printer->printFile($phpFile);
        FileHelper::writeFile($testQueryListFile, $testOut);

        // =========== GRAPHQL ===========
        // graphql files (create, update
        $selectionSet = self::getSelectionSet($typePhp);
        $mutationCreateOut = "mutation $mutationCreateGqlName(\$input: {$inputClassName}!) {
    $mutationCreateGqlName(input: \$input) {
$selectionSet
    }
}
";

        $mutationUpdateOut = "mutation $mutationUpdateGqlName(\${$entityLc}Id: Int!, \$input: {$inputClassName}!) {
    $mutationUpdateGqlName($entityLc: \${$entityLc}Id, input: \$input) {
$selectionSet
    }
}
";

        $queryListOut = "query {$queryListGqlName} {
    {$entityLc}s {
$selectionSet
    }
}
";

        $queryOut = "query {$queryGqlName} (\$$entityLc: Int!) {
    {$entityLc}($entityLc: \$$entityLc) {
$selectionSet
    }
}
";

        FileHelper::writeFile("{$testMutationDir}/{$mutationCreateClassName}.graphql", $mutationCreateOut);
        FileHelper::writeFile("{$testMutationDir}/{$mutationUpdateClassName}.graphql", $mutationUpdateOut);
        FileHelper::writeFile("{$testQueryDir}/{$queryListClassName}.graphql", $queryListOut);
        FileHelper::writeFile("{$testQueryDir}/{$queryClassName}.graphql", $queryOut);
    }

    private static function getInputConstructorExampleData(string $inputClass): string
    {
        /** @var class-string $className */
        $className = $inputClass;
        $rc = new ReflectionClass($className);

        $arguments = [];
        $constructor = $rc->getMethod('__construct');
        $parameters = $constructor->getParameters();
        foreach ($parameters as $parameter) {
            $parameterName = $parameter->getName();
            $type = $parameter->getType();
            if ($type === null) {
                continue;
            }
            $typeName = $type->__toString();

            if ($parameter->allowsNull()) {
                $arguments[] = "$parameterName: null,";
            } elseif ($typeName === 'UXF\Storage\Entity\Image' || $typeName === 'UXF\Storage\Entity\File' || $typeName === 'Ramsey\Uuid\UuidInterface') {
                $arguments[] = "$parameterName: \\Ramsey\\Uuid\\Uuid::fromString('00000000-0000-4000-0000-000000000001'),";
            } elseif (preg_match('/\\\Entity\\\/m', $typeName) === 1) {
                $arguments[] = "$parameterName: 1,";
            } elseif ($typeName === 'UXF\Core\Type\Email') {
                $arguments[] = "$parameterName: \\UXF\\Core\\Type\\Email::of('example@example.com'),";
            } elseif ($typeName === 'string') {
                $arguments[] = "$parameterName: 'Example string',";
            } elseif ($typeName === 'int') {
                $arguments[] = "$parameterName: 987654321,";
            } elseif ($typeName === 'boolean') {
                $arguments[] = "$parameterName: true,";
            } elseif ($typeName === 'array') {
                $arguments[] = "$parameterName: [],";
            } else {
                $arguments[] = "$parameterName: null, // TODO unknown type >$typeName< in code-gen package";
            }
        }
        return implode("\n", $arguments);
    }

    private static function getSelectionSet(string $inputClass): string
    {
        /** @var class-string $className */
        $className = $inputClass;
        $rc = new ReflectionClass($className);

        $selectionSet = [];
        $constructor = $rc->getMethod('__construct');
        $parameters = $constructor->getParameters();
        foreach ($parameters as $parameter) {
            $parameterName = $parameter->getName();
            $type = $parameter->getType();
            if ($type === null) {
                continue;
            }
            $typeName = $type->__toString();

            echo "{$parameterName}: {$typeName}\n";

            if (preg_match('/\\\GQL\\\Type\\\/m', $typeName) === 1) {
                $selectionSet[] = "        {$parameterName} {id}";
            } else {
                $selectionSet[] = "        {$parameterName}";
            }
        }
        return implode("\n", $selectionSet);
    }
}
