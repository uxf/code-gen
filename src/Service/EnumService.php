<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use UXF\CodeGen\Http\Request\EnumRequestBody;
use UXF\CodeGen\Util\FileHelper;

use function Safe\mkdir;

final readonly class EnumService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(EnumRequestBody $enumRequestBody): void
    {
        $zoneName = $enumRequestBody->zoneName;
        $enumName = $enumRequestBody->enumName;
        $enumItems = $enumRequestBody->enumItems;

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Enum");

        $enum = $namespace->addEnum($enumName);

        foreach ($enumItems as $enumItem) {
            $enumItem = strtoupper(str_replace(' ', '_', $enumItem));
            $enum->addCase($enumItem, $enumItem);
        }

        // print file
        $printer = new PsrPrinter();
        $out = $printer->printFile($phpFile);

        $dstDir = $this->srcDirectory . "{$zoneName}/Enum";
        $dstFile = "{$dstDir}/{$enumName}.php";

        if (!is_dir($dstDir)) {
            mkdir($dstDir, 0755, true);
        }

        FileHelper::writeFile($dstFile, $out);
    }
}
