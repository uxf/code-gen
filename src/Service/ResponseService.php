<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use ReflectionClass;
use ReflectionNamedType;
use UXF\CodeGen\Http\Request\ResponseRequestBody;
use UXF\CodeGen\Util\EntityHelper;
use UXF\CodeGen\Util\FileHelper;

use UXF\CodeGen\Util\ParseHelper;
use UXF\Core\Type\Date;
use UXF\Core\Type\DateTime;
use UXF\Core\Type\Time;
use UXF\Storage\Http\Response\FileResponse;
use UXF\Storage\Http\Response\ImageResponse;
use function Safe\mkdir;
use function Safe\preg_match;

final readonly class ResponseService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(ResponseRequestBody $responseRequestBody): void
    {
        $zoneName = $responseRequestBody->zoneName;
        $entityName = $responseRequestBody->entityName;
        $entityData = $this->processEntityData($zoneName, $entityName);
        $className = "{$entityName}Response";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Http\Response");

        $namespace
            ->addUse("App\\$zoneName\Entity\\$entityName")
            ->addUse(DateTime::class)
            ->addUse(Date::class)
            ->addUse(Time::class);

        foreach ($entityData as $entityRecord) {
            $returnType = $entityRecord['returnType'];
            $isResponse = $entityRecord['isResponse'];
            if ($isResponse) {
                $namespace->addUse($returnType);
            }
        }

        $class = $namespace
            ->addClass($className);
        $class->setFinal();

        // create constructor with promoted parameters
        $constructor = $class
            ->addMethod('__construct')
            ->setPublic();

        foreach ($entityData as $entityRecord) {
            $propertyName = $entityRecord['propertyName'];
            $returnType = $entityRecord['returnType'];
            $returnNullable = $entityRecord['returnNullable'];
            $constructor
                ->addPromotedParameter($propertyName)
                ->setType($returnType)
                ->setNullable($returnNullable)
                ->setReadOnly();
        }

        // create method "createFromEntity"
        $createFromEntity = $class
            ->addMethod('createFromEntity')
            ->setPublic()
            ->setStatic()
            ->setReturnType('self');

        $entityLc = lcfirst($entityName);
        $createFromEntity->addParameter($entityLc)
            ->setType("\App\\$zoneName\Entity\\$entityName");

        $properties = [];
        foreach ($entityData as $entityRecord) {
            $getterName = $entityRecord['getterName'];
            $returnType = $entityRecord['returnType'];
            $isResponse = $entityRecord['isResponse'];
            $returnNullable = $entityRecord['returnNullable'];
            if ($isResponse) {
                if ($returnNullable) {
                    $properties[] = "    \\$returnType::createNullable(\$$entityLc->$getterName()),";
                } else {
                    $properties[] = "    \\$returnType::createFromEntity(\$$entityLc->$getterName()),";
                }
            } else {
                $properties[] = "    \$$entityLc->$getterName(),";
            }
        }
        $createFromEntity->addBody("return new self(\n" . implode("\n", $properties) . "\n);");

        // create method "createNullable"
        $createNullable = $class
            ->addMethod('createNullable')
            ->setPublic()
            ->setStatic()
            ->setReturnNullable()
            ->setReturnType('self');

        $createNullable->addParameter($entityLc)
            ->setNullable()
            ->setType("\App\\$zoneName\Entity\\$entityName");

        $createNullable->addBody("return \$$entityLc !== null ? self::createFromEntity(\$$entityLc) : null;");

        // print class to file
        $printer = new PsrPrinter();
        $repositoryOut = $printer->printFile($phpFile);

        $responseDir = $this->srcDirectory . "{$zoneName}/Http/Response";
        if (!is_dir($responseDir)) {
            mkdir($responseDir, 0755, true);
        }

        $dstFile = "{$responseDir}/{$entityName}Response.php";
        FileHelper::writeFile($dstFile, $repositoryOut);
    }

    /**
     * @return array<array{getterName: string, propertyName: string, returnType: string, returnNullable: bool, returnBuiltin: bool, isResponse: bool}>
     */
    private function processEntityData(string $zoneName, string $entityName): array
    {
        /** @var class-string $className */
        $className = "App\\$zoneName\Entity\\$entityName";

        $rc = new ReflectionClass($className);

        $methods = $rc->getMethods();
        $methods = array_filter($methods, static fn ($method) => str_starts_with($method->getName(), 'get'));
        $entityData = [];
        foreach ($methods as $method) {
            $getterName = $method->getName();
            $propertyName = ParseHelper::convertGetterToPropertyName($getterName);
            $returnType = $method->getReturnType();
            if (!$returnType instanceof ReflectionNamedType) {
                continue;
            }

            if (EntityHelper::supportedReturnType($returnType) === false) {
                continue;
            }

            if ($returnType->getName() === 'UXF\Storage\Entity\Image') {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => ImageResponse::class,
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => true,
                ];
            } elseif ($returnType->getName() === 'UXF\Storage\Entity\File') {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => FileResponse::class,
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => true,
                ];
            } elseif (preg_match('/\\\Entity\\\/m', $returnType->getName()) === 1) {
                // je to entita, vazba ManyToOne , musim se podivat zda neexistuje Response pro tuto entitu
                // pokud existuje, pouzij Response, pokud neexistuje vytvor novou response
                echo "##### Processing entity: $className\n";

                $zoneName = ParseHelper::parseZoneNameFromClassName($returnType->getName());
                $entityName = ParseHelper::parseEntityFromClassName($returnType->getName());
                echo "- $getterName / $propertyName / $returnType / $zoneName / $entityName\n";

                $expectedResponseFile = $this->srcDirectory . "$zoneName/Http/Response/{$entityName}Response.php";

                if (!is_file($expectedResponseFile)) {
                    $this->generate(new ResponseRequestBody($zoneName, $entityName));
                }
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => "App\\$zoneName\\Http\\Response\\{$entityName}Response",
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => true,
                ];
            } else {
                $entityData[] = [
                    'getterName' => $getterName,
                    'propertyName' => $propertyName,
                    'returnType' => $returnType->getName(),
                    'returnNullable' => $returnType->allowsNull(),
                    'returnBuiltin' => $returnType->isBuiltin(),
                    'isResponse' => false,
                ];
            }
        }

        return $entityData;
    }
}
