<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Doctrine\ORM\EntityManagerInterface;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use UXF\CodeGen\Http\Request\RepositoryRequestBody;
use UXF\CodeGen\Util\FileHelper;
use function Safe\mkdir;

final readonly class RepositoryService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(RepositoryRequestBody $repositoryRequestBody): void
    {
        $zoneName = $repositoryRequestBody->zoneName;
        $entityName = $repositoryRequestBody->entityName;

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Repository");

        $namespace->addUse("App\\$zoneName\Entity\\$entityName")
            //->addUse(EntityManagerInterface::class);
            ->addUse(EntityManagerInterface::class);

        $class = $namespace->addClass("{$entityName}Repository");
        $class->setFinal()->setReadOnly();

        // constructor
        $class->addMethod('__construct')
            ->setPublic()
            ->addPromotedParameter('entityManager')
            ->setType(EntityManagerInterface::class)
            ->setPrivate();

        // add method findAll
        $class->addMethod('findAll')
            ->setPublic()
            ->setReturnType('array')
            ->setBody("
                return \$this->entityManager->createQueryBuilder()
                    ->select('e')
                    ->from($entityName::class, 'e')
                    ->orderBy('e.id', 'ASC')
                    ->getQuery()
                    ->getResult();")
            ->addComment("@return {$entityName}[]");

        // print file
        $printer = new PsrPrinter();
        $repositoryOut = $printer->printFile($phpFile);

        $repositoryDir = $this->srcDirectory . "{$zoneName}/Repository";
        if (!is_dir($repositoryDir)) {
            mkdir($repositoryDir, 0755, true);
        }

        $dstFile = "{$repositoryDir}/{$entityName}Repository.php";
        FileHelper::writeFile($dstFile, $repositoryOut);
    }
}
