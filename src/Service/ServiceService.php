<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Doctrine\ORM\EntityManagerInterface;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use ReflectionClass;
use ReflectionNamedType;
use TheCodingMachine\GraphQLite\Annotations\Query;
use UXF\CodeGen\Http\Request\QueryRequestBody;
use UXF\CodeGen\Util\EntityHelper;
use UXF\CodeGen\Util\FileHelper;
use UXF\CodeGen\Util\ParseHelper;
use UXF\Core\Attribute\Entity;
use function Safe\mkdir;
use function Safe\preg_match;

final readonly class ServiceService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(QueryRequestBody $queryRequestBody): void
    {
        $zoneName = $queryRequestBody->zoneName;
        $entityName = $queryRequestBody->entityName;

        // names
        $entityLc = lcfirst($entityName);
        $entityClass = "App\\$zoneName\Entity\\$entityName";

        $typeName = $entityName;
        $inputPhp = "\App\\{$zoneName}\\GQL\Input\\{$typeName}Input";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\Service");

        $namespace
            ->addUse($entityClass)
            ->addUse($inputPhp)
            ->addUse(EntityManagerInterface::class)
            ->addUse(Query::class)
            ->addUse(Entity::class);

        $class = $namespace->addClass("{$entityName}Service")->setFinal()->setReadOnly();

        // constructor
        $class->addMethod('__construct')
            ->setPublic()
            ->addPromotedParameter('entityManager')
            ->setPrivate()
            ->setType(EntityManagerInterface::class);

        // create method
        $arguments = self::getCreateBodyEntityArguments($entityClass);
        $createMethod = $class->addMethod('create')
            ->setPublic()
            ->setReturnType($entityClass)
            ->setBody(
                <<<PHP
        \$$entityLc = new $entityName(
            $arguments
        );

        \$this->entityManager->persist(\$$entityLc);
        \$this->entityManager->flush();

        return \$$entityLc;
PHP
            );
        $createMethod
            ->addParameter("input")
            ->setType($inputPhp);

        // update method
        $settersBody = self::getUpdateBodyEntityArguments($entityClass, $entityLc);
        $updateMethod = $class->addMethod('update')
            ->setPublic()
            ->setReturnType($entityClass)
            ->setBody(
                <<<PHP
        $settersBody
        \$this->entityManager->flush();

        return \$$entityLc;
PHP
            );

        $updateMethod
            ->addParameter($entityLc)
            ->setType($entityClass);

        $updateMethod
            ->addParameter("input")
            ->setType($inputPhp);

        // print file
        $printer = new PsrPrinter();
        $queryOut = $printer->printFile($phpFile);

        $queryDir = $this->srcDirectory . "{$zoneName}/Service";
        if (!is_dir($queryDir)) {
            mkdir($queryDir, 0755, true);
        }

        $dstFile = "{$queryDir}/{$entityName}Service.php";
        FileHelper::writeFile($dstFile, $queryOut);
    }

    private static function getCreateBodyEntityArguments(string $entityClass): string
    {
        /** @var class-string $className */
        $className = $entityClass;
        $rc = new ReflectionClass($className);

        $arguments = [];
        $constructor = $rc->getMethod('__construct');
        $parameters = $constructor->getParameters();
        foreach ($parameters as $parameter) {
            $parameterName = $parameter->getName();
            $type = $parameter->getType();
            if ($type === null) {
                continue;
            }
            $typeName = $type->__toString();
            if ($typeName === 'UXF\Storage\Entity\Image' || $typeName === 'UXF\Storage\Entity\File') {
                $arguments[] = "$parameterName: null, // TODO find by UUID \$input->{$parameterName}Uuid";
            } elseif (preg_match('/\\\Entity\\\/m', $typeName) === 1) {
                $typeName = str_replace('?', '', $typeName);
                $value = "\$this->entityManager->find(\\{$typeName}::class, \$input->{$parameterName}Id ?? 0)";
                $arguments[] = "$parameterName: $value,";
            } else {
                $arguments[] = "$parameterName: \$input->{$parameterName},";
            }
        }
        return implode("\n", $arguments);
    }

    private static function getUpdateBodyEntityArguments(string $entityClass, string $entityLc): string
    {
        /** @var class-string $className */
        $className = $entityClass;
        $rc = new ReflectionClass($className);
        $methods = $rc->getMethods();
        $methods = array_filter($methods, static fn ($method) => str_starts_with($method->getName(), 'set'));
        $arguments = [];
        foreach ($methods as $method) {
            $setterName = $method->getName();
            $propertyName = ParseHelper::convertSetterToPropertyName($setterName);

            $param = $method->getParameters()[0];
            $type = $param->getType();

            if (!$type instanceof ReflectionNamedType) {
                continue;
            }
            if (EntityHelper::supportedReturnType($type) === false) {
                continue;
            }
            $typeName = $type->getName();

            if ($typeName === 'UXF\Storage\Entity\Image' || $typeName === 'UXF\Storage\Entity\File') {
                $arguments[] = "// TODO find File by UUID \$$entityLc->$setterName(\$input->$propertyName);";
            } elseif (preg_match('/\\\Entity\\\/m', $typeName) === 1) {
                $typeName = str_replace('?', '', $typeName);
                $value = "\$this->entityManager->find(\\{$typeName}::class, \$input->{$propertyName}Id ?? 0)";
                $arguments[] = "\$$entityLc->$setterName($value);";
            } else {
                $arguments[] = "\$$entityLc->$setterName(\$input->$propertyName);";
            }
        }


        return implode("\n", $arguments);
    }
}
