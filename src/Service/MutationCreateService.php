<?php

declare(strict_types=1);

namespace UXF\CodeGen\Service;

use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use UXF\CodeGen\Http\Request\QueryRequestBody;
use UXF\CodeGen\Util\FileHelper;
use UXF\Core\Attribute\Entity;
use function Safe\mkdir;

final readonly class MutationCreateService
{
    public function __construct(private string $srcDirectory)
    {
    }

    public function generate(QueryRequestBody $queryRequestBody): void
    {
        $zoneName = $queryRequestBody->zoneName;
        $entityName = $queryRequestBody->entityName;

        // names
        $entityLc = lcfirst($entityName);
        $entityClass = "App\\$zoneName\Entity\\$entityName";

        $typeName = "{$entityName}Type";
        $typePhp = "\App\\{$zoneName}\\GQL\Type\\{$typeName}";
        $inputPhp = "\App\\{$zoneName}\\GQL\Input\\{$entityName}Input";
        $servicePhp = "\App\\{$zoneName}\\Service\\{$entityName}Service";

        // generate output php class
        $phpFile = new PhpFile();
        $phpFile->setStrictTypes();
        $namespace = $phpFile->addNamespace("App\\$zoneName\GQL\Mutation");

        $namespace->addUse($entityClass)
            ->addUse($typePhp)
            ->addUse($inputPhp)
            ->addUse($servicePhp)
            ->addUse(Mutation::class)
            ->addUse(Entity::class);

        $class = $namespace->addClass("{$entityName}CreateMutation")->setFinal()->setReadOnly();

        // constructor
        $constructor = $class->addMethod('__construct')
            ->setPublic();

        $constructor
            ->addPromotedParameter("{$entityLc}Service")
            ->setType($servicePhp)
            ->setPrivate();

        // index
        $method = $class->addMethod('__invoke')
            ->setPublic()
            ->setReturnType($typePhp)
            ->setBody("
                \$$entityLc = \$this->{$entityLc}Service->create(\$input);
                
                return $typeName::createFromEntity(\$$entityLc);")
            ->addAttribute(Mutation::class, [
                'name' => "{$entityLc}Create",
            ]);

        $method
            ->addParameter("input")
            ->setType($inputPhp);

        // print file
        $printer = new PsrPrinter();
        $queryOut = $printer->printFile($phpFile);

        $queryDir = $this->srcDirectory . "{$zoneName}/GQL/Mutation";
        if (!is_dir($queryDir)) {
            mkdir($queryDir, 0755, true);
        }

        $dstFile = "{$queryDir}/{$entityName}CreateMutation.php";
        FileHelper::writeFile($dstFile, $queryOut);
    }
}
