<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use SebastianBergmann\CodeCoverage\CodeCoverage;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Route;

#[AsCommand(name: 'uxf:code-gen:uncovered-controller', description: 'Check Controller test coverage')]
class UncoveredControllerCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(['<comment>Check Controller coverage', '=========================', '</comment>']);

        if (!file_exists(GeneratorBase::PHP_COVERAGE_REPORT_FILENAME)) {
            $output->writeln(
                '<error>Coverage report "' .
                GeneratorBase::PHP_COVERAGE_REPORT_FILENAME . '" not found. Run `make coveragexdebug`.</error>',
            );
            return 0;
        }

        $appControllers = GeneratorBase::getAppControllers($this->router); // get all app controllers from router

        /** @var CodeCoverage $unserialized */
        $unserialized = require GeneratorBase::PHP_COVERAGE_REPORT_FILENAME; // @phpstan-ignore require.fileNotFound

        $report = $unserialized->getReport();

        $uncoveredControllers = [];
        GeneratorBase::getUncoveredControllers($report, $uncoveredControllers);

        $this->viewReport($uncoveredControllers, $appControllers, $output);

        return Command::SUCCESS;
    }

    /**
     * @param array<mixed> $uncoveredControllers
     * @param array<string, Route> $appControllers
     */
    private function viewReport(array $uncoveredControllers, array $appControllers, OutputInterface $output): void
    {
        foreach ($uncoveredControllers as $item) {
            /** @var Route $route */
            $route = $appControllers[$item['className']];

            $output->writeln('Class: ' . $item['className'] . ' Coverage: ' . $item['coverage']);
            $output->writeln('  Route details: ');
            $output->writeln("      - path: " . $route->getPath());
            $output->writeln("      - method: " . ($route->getMethods()[0] ?? ''));
            $output->writeln("      - requirements: ");
            foreach ($route->getRequirements() as $requirementName => $requirementValue) {
                $output->writeln("          * $requirementName: $requirementValue");
            }
        }
    }
}
