<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CodeGen\Http\Request\ResponseRequestBody;

#[AsCommand(name: 'uxf:code-gen:http-response', description: 'Generate HTTP response from given Entity')]
class HttpResponseGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Select Zone name
        $zoneName = $this->selectZone($input, $output);

        // Select entity name
        $entityName = $this->selectEntity($zoneName, $input, $output);

        $this->responseService->generate(new ResponseRequestBody($zoneName, $entityName));

        return 0;
    }
}
