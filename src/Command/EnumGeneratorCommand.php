<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use UXF\CodeGen\Http\Request\EnumRequestBody;

#[AsCommand(name: 'uxf:code-gen:enum', description: 'Generate ENUM in zone')]
class EnumGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Select Zone name
        $zoneName = $this->selectZone($input, $output);

        // Ask for ENUM name
        $helper = $this->getHelper('question');
        assert($helper instanceof QuestionHelper);
        $question = new Question('Please enter a ENUM name: ', '');

        $enumName = $helper->ask($input, $output, $question);

        // Enter enum items
        $enumItems = [];
        while (true) {
            $output->writeln('');
            $question = new Question('Please enter a enum item name (eg. name; ENTER = end): ', '');
            $enumItem = $helper->ask($input, $output, $question);

            // exit if column name is empty
            if (!(bool) $enumItem) {
                break;
            }

            $enumItems[] = $enumItem;
        }

        $this->enumService->generate(new EnumRequestBody($zoneName, $enumName, $enumItems));

        return 0;
    }
}
