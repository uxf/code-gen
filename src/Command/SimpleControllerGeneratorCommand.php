<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CodeGen\Http\Request\ControllerRequestBody;

#[AsCommand(name: 'uxf:code-gen:simple-controller', description: 'Generate simple controller for entity in Zone')]
class SimpleControllerGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Select Zone name
        $zoneName = $this->selectZone($input, $output);

        // Select entity name
        $entityName = $this->selectEntity($zoneName, $input, $output);

        $this->controllerService->generate(new ControllerRequestBody($zoneName, $entityName));

        return 0;
    }
}
