<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use SebastianBergmann\CodeCoverage\Node\Directory;
use SebastianBergmann\CodeCoverage\Node\File;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;
use UXF\CodeGen\Service\ControllerService;
use UXF\CodeGen\Service\EntityService;
use UXF\CodeGen\Service\EnumService;
use UXF\CodeGen\Service\GqlTestService;
use UXF\CodeGen\Service\GridService;
use UXF\CodeGen\Service\InputService;
use UXF\CodeGen\Service\MutationCreateService;
use UXF\CodeGen\Service\MutationUpdateService;
use UXF\CodeGen\Service\QueryListService;
use UXF\CodeGen\Service\QueryService;
use UXF\CodeGen\Service\RepositoryService;
use UXF\CodeGen\Service\ResponseService;
use UXF\CodeGen\Service\ServiceService;
use UXF\CodeGen\Service\TypeService;
use UXF\CodeGen\Service\ZoneService;

use function Safe\preg_match;
use function Safe\scandir;

abstract class GeneratorBase extends Command
{
    protected const string PHP_COVERAGE_REPORT_FILENAME = './_reports/report-coverage.php';
    protected const int MINIMUM_COVERAGE = 80;

    public function __construct(
        protected readonly string $projectDirectory,
        protected readonly string $srcDirectory,
        protected readonly RouterInterface $router,
        protected readonly ZoneService $zoneService,
        protected readonly EntityService $entityService,
        protected readonly RepositoryService $repositoryService,
        protected readonly ResponseService $responseService,
        protected readonly ControllerService $controllerService,
        protected readonly InputService $inputService,
        protected readonly GqlTestService $gqlTestService,
        protected readonly MutationUpdateService $mutationUpdateService,
        protected readonly MutationCreateService $mutationCreateService,
        protected readonly ServiceService $serviceService,
        protected readonly QueryService $queryService,
        protected readonly QueryListService $queryListService,
        protected readonly TypeService $typeService,
        protected readonly EnumService $enumService,
        protected readonly GridService $gridService,
    ) {
        parent::__construct();
    }

    /**
     * @return string[]
     */
    protected function getEntities(string $zoneName): array
    {
        $dirs = scandir($this->srcDirectory . $zoneName . '/Entity');

        $entities = [];
        foreach ($dirs as $dir) {
            if (($dir !== '.') && ($dir !== '..')) {
                $entities[] = str_replace('.php', '', $dir);
            }
        }

        return $entities;
    }

    protected function selectZone(InputInterface $input, OutputInterface $output): string
    {
        $zones = $this->zoneService->getZones();

        $helper = $this->getHelper('question');
        assert($helper instanceof QuestionHelper);
        $question = new ChoiceQuestion('Choose Zone: ', $zones, 0);
        $question->setErrorMessage('Zone %s is invalid.');

        return $helper->ask($input, $output, $question);
    }

    protected function selectEntity(string $zoneName, InputInterface $input, OutputInterface $output): string
    {
        $entities = $this->getEntities($zoneName);

        $helper = $this->getHelper('question');
        assert($helper instanceof QuestionHelper);
        $question = new ChoiceQuestion('Choose Entity: ', $entities, 0);
        $question->setErrorMessage('Entity %s is invalid.');

        return str_replace('.php', '', $helper->ask($input, $output, $question));
    }

    /**
     * @return array<string, Route>
     */
    public static function getAppControllers(RouterInterface $router): array
    {
        $routes = $router->getRouteCollection()->getIterator();

        $controllers = [];

        foreach ($routes as $route) {
            foreach ($route->getDefaults() as $key => $className) {
                $className = (string)$className;
                if ($key === '_controller' && str_starts_with($className, 'App')) {
                    $controllers[$className] = $route;
                }
            }
        }

        return $controllers;
    }

    /**
     * @param Directory<Directory|File> $directory
     * @param array<mixed> $uncoveredControllers
     */
    public static function getUncoveredControllers(Directory $directory, array &$uncoveredControllers): void
    {
        foreach ($directory->children() as $children) {
            if ($children instanceof Directory) {
                self::getUncoveredControllers($children, $uncoveredControllers);
            } elseif ($children instanceof File) {
                $classes = $children->classes();

                foreach ($classes as $className => $class) {
                    if ($class['coverage'] < self::MINIMUM_COVERAGE && preg_match('/Controller$/', $className) === 1) {
                        $uncoveredControllers[] = [
                            'className' => $className,
                            'coverage' => $class['coverage'],
                        ];
                    }
                }
            }
        }
    }
}
