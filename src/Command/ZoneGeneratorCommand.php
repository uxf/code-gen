<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use UXF\CodeGen\Http\Request\ZoneRequestBody;
use UXF\CodeGen\Service\ZoneService;

#[AsCommand(name: 'uxf:code-gen:zone', description: 'Generate Zone directories')]
class ZoneGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');
        assert($helper instanceof QuestionHelper);
        $question = new Question('Please enter a zone name (eg. ProfileZone): ', '');

        $zoneName = $helper->ask($input, $output, $question);

        $availableSubDirs = ZoneService::getAvailableSubDirs();

        $newDirs = [];
        foreach ($availableSubDirs as $dir) {
            $newDir = $this->srcDirectory . "$zoneName/$dir";
            if (!is_dir($newDir)) {
                $question = new ConfirmationQuestion("Create directory '$zoneName/$dir'? (y/n) ", true);

                if ((bool) $helper->ask($input, $output, $question)) {
                    $newDirs[] = $dir;
                }
            }
        }

        $this->zoneService->generate(new ZoneRequestBody($zoneName, $newDirs));

        return 0;
    }
}
