<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CodeGen\Http\Request\QueryRequestBody;
use UXF\CodeGen\Http\Request\RepositoryRequestBody;
use UXF\CodeGen\Http\Request\TypeRequestBody;

#[AsCommand(name: 'uxf:code-gen:gql-all', description: 'Generate simple repository, service, GQL input, type, mutation, query and story test for Entity in Zone')]
class GqlAllGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $zoneName = $this->selectZone($input, $output);
        $entityName = $this->selectEntity($zoneName, $input, $output);

        $this->repositoryService->generate(new RepositoryRequestBody($zoneName, $entityName));
        $this->serviceService->generate(new QueryRequestBody($zoneName, $entityName));
        $this->inputService->generate(new TypeRequestBody($zoneName, $entityName));
        $this->typeService->generate(new TypeRequestBody($zoneName, $entityName));
        $this->queryService->generate(new QueryRequestBody($zoneName, $entityName));
        $this->queryListService->generate(new QueryRequestBody($zoneName, $entityName));
        $this->mutationCreateService->generate(new QueryRequestBody($zoneName, $entityName));
        $this->mutationUpdateService->generate(new QueryRequestBody($zoneName, $entityName));
        $this->gqlTestService->generate(new QueryRequestBody($zoneName, $entityName));

        return 0;
    }
}
