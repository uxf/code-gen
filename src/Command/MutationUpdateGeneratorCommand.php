<?php

declare(strict_types=1);

namespace UXF\CodeGen\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UXF\CodeGen\Http\Request\QueryRequestBody;
use UXF\CodeGen\Http\Request\TypeRequestBody;

#[AsCommand(name: 'uxf:code-gen:mutation-update', description: 'Generate simple GQL update mutation for entity in Zone')]
class MutationUpdateGeneratorCommand extends GeneratorBase
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $zoneName = $this->selectZone($input, $output);
        $entityName = $this->selectEntity($zoneName, $input, $output);

        $this->inputService->generate(new TypeRequestBody($zoneName, $entityName));
        $this->mutationUpdateService->generate(new QueryRequestBody($zoneName, $entityName));

        return 0;
    }
}
