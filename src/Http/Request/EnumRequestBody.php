<?php

declare(strict_types=1);

namespace UXF\CodeGen\Http\Request;

final readonly class EnumRequestBody
{
    /**
     * @param string[] $enumItems
     */
    public function __construct(
        public string $zoneName,
        public string $enumName,
        public array $enumItems,
    ) {
    }
}
