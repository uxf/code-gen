<?php

declare(strict_types=1);

namespace UXF\CodeGen\Http\Request;

final readonly class ZoneRequestBody
{
    /**
     * @param string[] $subDirs
     */
    public function __construct(
        public string $zoneName,
        public array $subDirs,
    ) {
    }
}
