<?php

declare(strict_types=1);

namespace UXF\CodeGen\Http\Request;

final readonly class GridRequestBody
{
    public function __construct(
        public string $zoneName,
        public string $entityName,
    ) {
    }
}
