<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use UXF\CodeGen\Command\ControllerTestGeneratorCommand;
use UXF\CodeGen\Command\EntityGeneratorCommand;
use UXF\CodeGen\Command\EnumGeneratorCommand;
use UXF\CodeGen\Command\GenerateFromCSVCommand;
use UXF\CodeGen\Command\GqlAllGeneratorCommand;
use UXF\CodeGen\Command\GqlTestGeneratorCommand;
use UXF\CodeGen\Command\GridGeneratorCommand;
use UXF\CodeGen\Command\HttpResponseGeneratorCommand;
use UXF\CodeGen\Command\InputGeneratorCommand;
use UXF\CodeGen\Command\MutationCreateGeneratorCommand;
use UXF\CodeGen\Command\MutationUpdateGeneratorCommand;
use UXF\CodeGen\Command\QueryGeneratorCommand;
use UXF\CodeGen\Command\QueryListGeneratorCommand;
use UXF\CodeGen\Command\RepositoryGeneratorCommand;
use UXF\CodeGen\Command\ServiceGeneratorCommand;
use UXF\CodeGen\Command\SimpleControllerGeneratorCommand;
use UXF\CodeGen\Command\TypeGeneratorCommand;
use UXF\CodeGen\Command\UncoveredControllerCommand;
use UXF\CodeGen\Command\ZoneGeneratorCommand;
use UXF\CodeGen\Service\ControllerService;
use UXF\CodeGen\Service\EntityService;
use UXF\CodeGen\Service\EnumService;
use UXF\CodeGen\Service\GqlTestService;
use UXF\CodeGen\Service\GridService;
use UXF\CodeGen\Service\InputService;
use UXF\CodeGen\Service\MutationCreateService;
use UXF\CodeGen\Service\MutationUpdateService;
use UXF\CodeGen\Service\QueryListService;
use UXF\CodeGen\Service\QueryService;
use UXF\CodeGen\Service\RepositoryService;
use UXF\CodeGen\Service\ResponseService;
use UXF\CodeGen\Service\ServiceService;
use UXF\CodeGen\Service\TypeService;
use UXF\CodeGen\Service\ZoneService;

return static function (ContainerConfigurator $containerConfigurator): void {
    $services = $containerConfigurator->services();

    $services->defaults()
        ->bind('$projectDirectory', '%kernel.project_dir%')
        ->bind('$srcDirectory', '%kernel.project_dir%/src/')
        ->autowire()
        ->autoconfigure();

    $services->set(EntityGeneratorCommand::class);
    $services->set(EnumGeneratorCommand::class);
    $services->set(HttpResponseGeneratorCommand::class);
    $services->set(RepositoryGeneratorCommand::class);
    $services->set(SimpleControllerGeneratorCommand::class);
    $services->set(ZoneGeneratorCommand::class);
    $services->set(ControllerTestGeneratorCommand::class);
    $services->set(UncoveredControllerCommand::class);
    $services->set(GenerateFromCSVCommand::class);
    $services->set(ZoneService::class);
    $services->set(EntityService::class);
    $services->set(EnumService::class);
    $services->set(RepositoryService::class);
    $services->set(ResponseService::class);
    $services->set(ControllerService::class);
    $services->set(GridGeneratorCommand::class);
    $services->set(InputGeneratorCommand::class);
    $services->set(GqlTestGeneratorCommand::class);
    $services->set(GqlAllGeneratorCommand::class);
    $services->set(MutationCreateGeneratorCommand::class);
    $services->set(MutationUpdateGeneratorCommand::class);
    $services->set(ServiceGeneratorCommand::class);
    $services->set(QueryGeneratorCommand::class);
    $services->set(QueryListGeneratorCommand::class);
    $services->set(TypeGeneratorCommand::class);
    $services->set(GridService::class);
    $services->set(InputService::class);
    $services->set(GqlTestService::class);
    $services->set(MutationUpdateService::class);
    $services->set(MutationCreateService::class);
    $services->set(ServiceService::class);
    $services->set(QueryService::class);
    $services->set(QueryListService::class);
    $services->set(TypeService::class);
};
