# CodeGen

Simple Code Generator for UXF projects. UXF projects are based on PHP Symfony and Doctrine and 
has custom directory structure similar to DDD.

## Install
```
$ composer require --dev uxf/code-gen
```

## Usage

`./bin/console ...`
```
      uxf:code-gen:entity               Generate Doctrine Entity in given Zone                                                                   
      uxf:code-gen:enum                 Generate ENUM in zone                                                                                    
      uxf:code-gen:http-response        Generate HTTP response from given Entity                                                                 
      uxf:code-gen:repository           Generate Doctrine Repository for given Entity                                                            
      uxf:code-gen:simple-controller    Generate simple controller for entity in Zone                                                            
      uxf:code-gen:zone                 Generate Zone directories                                                                                
      uxf:code-gen:controller-test      Check Controller coverage                                                                                
      uxf:code-gen:uncovered-controller Check Controller test coverage                                                                           
      uxf:code-gen:generate-from-csv    Generate from CSV file                                                                                   
      uxf:code-gen:grid                 Generate Grid for entity in Zone                                                                         
      uxf:code-gen:input                Generate simple GQL input for entity in Zone                                                             
      uxf:code-gen:gql-test             Generate simple GQL story test for entity in Zone                                                        
      uxf:code-gen:gql-all              Generate simple repository, service, GQL input, type, mutation, query and story test for Entity in Zone  
      uxf:code-gen:mutation-update      Generate simple GQL update mutation for entity in Zone                                                   
      uxf:code-gen:service              Generate simple Service for entity in Zone                                                               
      uxf:code-gen:query                Generate simple GQL query for entity in Zone                                                             
      uxf:code-gen:query-list           Generate simple GQL query list for entity in Zone                                                        
      uxf:code-gen:type                 Generate GQL type for entity in Zone.   
```
