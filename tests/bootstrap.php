<?php

declare(strict_types=1);

use Symfony\Component\ErrorHandler\ErrorHandler;

require __DIR__ . '/../../../vendor/autoload.php';

// https://github.com/symfony/symfony/issues/53812#issuecomment-1962311843
ErrorHandler::register(null, false);
