<?php

declare(strict_types=1);

namespace App\Zoo\Entity;

use Doctrine\ORM\Mapping as ORM;
use UXF\CMS\Attribute\CmsForm;
use UXF\CMS\Attribute\CmsGrid;
use UXF\CMS\Attribute\CmsMeta;

#[ORM\Entity]
#[ORM\Table(schema: 'app_zoo')]
#[CmsMeta(alias: 'animal', title: 'Label')]
class Animal
{
    #[ORM\Column]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[CmsGrid(label: 'Id')]
    #[CmsForm(hidden: true)]
    private ?int $id = 0;

    #[ORM\Column(type: 'string', nullable: false)]
    #[CmsGrid(label: 'Name')]
    #[CmsForm(label: 'Name')]
    private string $name;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[CmsGrid(label: 'Number')]
    #[CmsForm(label: 'Number')]
    private ?int $number = null;

    #[ORM\Column(type: \UXF\Core\Type\DateTime::class, nullable: false)]
    #[CmsGrid(label: 'Datetime')]
    #[CmsForm(label: 'Datetime')]
    private \UXF\Core\Type\DateTime $datetime;

    #[ORM\Column(type: \UXF\Core\Type\Date::class, nullable: false)]
    #[CmsGrid(label: 'Date')]
    #[CmsForm(label: 'Date')]
    private \UXF\Core\Type\Date $date;

    #[ORM\ManyToOne(inversedBy: 'animals')]
    #[ORM\JoinColumn(nullable: false)]
    #[CmsGrid(label: 'Park', targetColumn: 'name')]
    #[CmsForm(label: 'Park', targetField: 'name')]
    private Park $park;

    public function __construct(
        string $name,
        \UXF\Core\Type\DateTime $datetime,
        \UXF\Core\Type\Date $date,
        Park $park,
    ) {
        $this->name = $name;
        $this->datetime = $datetime;
        $this->date = $date;
        $this->park = $park;
    }

    public function getId(): int
    {
        return $this->id ?? 0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;
        return $this;
    }

    public function getDatetime(): \UXF\Core\Type\DateTime
    {
        return $this->datetime;
    }

    public function setDatetime(\UXF\Core\Type\DateTime $datetime): self
    {
        $this->datetime = $datetime;
        return $this;
    }

    public function getDate(): \UXF\Core\Type\Date
    {
        return $this->date;
    }

    public function setDate(\UXF\Core\Type\Date $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function getPark(): Park
    {
        return $this->park;
    }

    public function setPark(Park $park): self
    {
        $this->park = $park;
        return $this;
    }
}
