<?php

declare(strict_types=1);

namespace UXF\CodeGenTests\Unit;

use Nette\Utils\FileSystem;
use PHPUnit\Framework\TestCase;
use UXF\CodeGen\Http\Request\EntityColumnRequestBody;
use UXF\CodeGen\Http\Request\EntityRequestBody;
use UXF\CodeGen\Service\EntityService;

class EntityServiceTest extends TestCase
{
    public function test(): void
    {
        $service = new EntityService(__DIR__ . '/../../var/src/');

        $entityDto = new EntityRequestBody(
            zoneName: 'Zoo',
            entityName: 'Animal',
            entityLabel: 'Label',
            columns: [
                new EntityColumnRequestBody(
                    name: 'name',
                    label: 'Name',
                    dbType: 'string',
                    nullable: false,
                ),
                new EntityColumnRequestBody(
                    name: 'number',
                    label: 'Number',
                    dbType: 'integer',
                    nullable: true,
                ),
                new EntityColumnRequestBody(
                    name: 'datetime',
                    label: 'Datetime',
                    dbType: 'datetime',
                    nullable: false,
                ),
                new EntityColumnRequestBody(
                    name: 'date',
                    label: 'Date',
                    dbType: 'date',
                    nullable: false,
                ),
                new EntityColumnRequestBody(
                    name: 'park',
                    label: 'Park',
                    dbType: 'ManyToOne',
                    nullable: false,
                    many2oneZoneName: 'Zoo',
                    many2oneEntityName: 'Park',
                ),
            ],
        );

        FileSystem::delete(__DIR__ . '/../../var/');
        FileSystem::createDir(__DIR__ . '/../../var/src/Zoo/Entity');
        $service->generate($entityDto);

        self::assertFileEquals(__DIR__ . '/expected/Animal.phpt', __DIR__ . '/../../var/src/Zoo/Entity/Animal.php');
    }
}
